from flask import Flask,render_template

app=Flask(__name__)

@app.route('/')
def a():
	return render_template('a.html')
@app.route('/xyz')
def index():
	return render_template('index.html')
if __name__=='__main__':
	app.run(debug=True)
