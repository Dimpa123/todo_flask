from flask import Flask,render_template

app=Flask(__name__)

@app.route('/')
def abc():
	return render_template('abc.html')
@app.route('/xyz')
def ab():
	return render_template('ab.html')
if __name__=='__main__':
	app.run(debug=True)
